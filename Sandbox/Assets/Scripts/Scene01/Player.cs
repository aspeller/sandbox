﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float speed = 10;

	// Update is called once per frame
	void Update () {

        // Get the input from the keyboard
        // Vector3 forward/backward, up/down, left/right
        Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

        // Normalize
        Vector3 direction = input.normalized;
        Vector3 velocity = direction * speed;

        // Smooth out the movement per second instead of per frame
        Vector3 moveAmount = velocity * Time.deltaTime;

        // Move the object attached to this script
        //transform.position += moveAmouont;
        // or
        transform.Translate(moveAmount);
	}
}
