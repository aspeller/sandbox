﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene2Player : MonoBehaviour {

    Rigidbody rb;
    public float speed = 6;
    Vector3 velocity;
    int coinCount;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

        Vector3 direction = input.normalized;
        velocity = direction * speed;
	}

    private void FixedUpdate()
    {
        rb.position += velocity * Time.fixedDeltaTime;
        // or
        // rb.position += velocity * Time.DeltaTime;
        // because Unity is smart enough to know that we are in the FixedUpdate method
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Coin")
        {
            Destroy(other.gameObject);
            coinCount++;
        }
    }
}
